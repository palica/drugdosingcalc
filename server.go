package main

import (
	"html/template"
	"net/http"
)

func process(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("tmpl.html")
	greeting := string("Hello World!")
	t.Execute(w, greeting)
}

func main() {
	server := http.Server{
		Addr: ":8080",
	}
	http.HandleFunc("/", process)
	server.ListenAndServe()
}
