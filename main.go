package main

import "fmt"

func dose(amount,pat_weight float64) float64 {
	return amount / pat_weight
}
func amount(dose,pat_weight float64) float64 {
	return dose * pat_weight
}

func main() {
	fmt.Println("Input patient weight:")
	var pat_weight float64
	fmt.Scanln(&pat_weight)
	fmt.Println("Patient weight:", pat_weight)

	fmt.Println("Input medicament that you want to give:")
	var drug string
	fmt.Scanln(&drug)
	fmt.Println("You entered", drug, "as the drug.")

	fmt.Println("Do you want to give total amount and let me tell you the dose? -> 1")
	fmt.Println("Do you want to give dose and let me tell you the amount? -> 2")
	var choice int
	fmt.Scanln(&choice)
// better would be select case
	if choice == 1 {
		fmt.Println("Enter amount of the drug", drug, "to give:")
		var amount float64
		var unit string
		fmt.Scanln(&amount)
		fmt.Println("Enter the unit")
		fmt.Scanln(&unit)

		fmt.Println("By giving", amount, unit, "of", drug, "to a", pat_weight, "kg patient, you would administer", dose(amount,pat_weight), unit+"/kg.")
	} else {
		fmt.Println("Enter dose of the drug", drug, "to give:")
		var dose float64
		var unit string
		fmt.Scanln(&dose)
		fmt.Println("Enter the unit")
		fmt.Scanln(&unit)

		fmt.Println("By giving", dose, unit+"/kg", "of", drug, "to a", pat_weight, "kg patient, you would administer", amount(dose,pat_weight), unit+" total.")
	}
// amount
// dose
// patient weight
}
